# SQL : TP 2 
* Léo STEVENOT
* Nathan PONCET
* Mohamed BENALIA

## Install
### Docker
#### Install step
> Install docker : https://docs.docker.com/engine/install/
#### Second step
```shell
bash docker-run.sh # Launch project
```
### Local
```shell
bash local-run.sh # Launch project
```

## Db
### Docker - PhpMyAdmin
> https://localhost:8081
### MLD
> https://dbdiagram.io/d/63871c55bae3ed7c4543bf07
> 

## Requests
