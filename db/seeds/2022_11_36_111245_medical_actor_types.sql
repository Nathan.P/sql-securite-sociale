INSERT INTO medical_actor_types (id, type)
VALUES
    (1, 'Hôpital'),
    (2, 'Généraliste'),
    (3, 'Spécialiste');