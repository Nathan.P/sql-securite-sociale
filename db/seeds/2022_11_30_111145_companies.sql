INSERT INTO companies (id, name, social_capital, address_id)
VALUES 
    (1, 'Dignissim Magna Institute', 643533, 2), 
    (2, 'Vestibulum Mauris PC', 115386, 3), 
    (3, 'Pede Associates', 754339, 8), 
    (4, 'Consequat Purus LLC', 694419, 45),
    (5, 'Et Foundation', 339116, 6),
    (6, 'Quam Corporation', 406086, 4),
    (7, 'Penatibus Inc.', 406242, 7),
    (8, 'Nulla Industries', 510835, 9),
    (9, 'Pede Cras Limited', 472560, 5),
    (10, 'Convallis In Industries', 286528, 10);
