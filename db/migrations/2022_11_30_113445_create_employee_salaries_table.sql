CREATE TABLE
    employee_salaries (
        id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        employee_id BIGINT NOT NULL,
        salary DECIMAL(10, 2) NOT NULL,
        FOREIGN KEY (employee_id) REFERENCES employees(id)
    );
