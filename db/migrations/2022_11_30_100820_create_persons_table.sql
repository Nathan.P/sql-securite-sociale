CREATE TABLE
    persons (
        id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        firstname VARCHAR(100) NOT NULL,
        lastname VARCHAR(100) NOT NULL,
        address_id BIGINT NOT NULL,
        is_retired BOOLEAN NOT NULL,
        FOREIGN KEY (address_id) REFERENCES addresses(id)
    );
