CREATE TABLE
    employees (
        id BIGINT NOT NULL PRIMARY KEY REFERENCES persons(id),
        status_id BIGINT NOT NULL,
        company_id BIGINT NOT NULL,
        salary DECIMAL(10, 2) NOT NULL,
        FOREIGN KEY (status_id) REFERENCES employee_statutes(id),
        FOREIGN KEY (company_id) REFERENCES companies(id)
    );
