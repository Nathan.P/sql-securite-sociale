CREATE TABLE
    companies (
        id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(100) NOT NULL,
        social_capital DECIMAL(15,2) NOT NULL,
        address_id BIGINT NOT NULL,
        FOREIGN KEY (address_id) REFERENCES addresses(id)
    );