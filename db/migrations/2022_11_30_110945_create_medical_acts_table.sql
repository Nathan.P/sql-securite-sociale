CREATE TABLE
    medical_acts (
        id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        actor_type_id BIGINT NOT NULL,
        name VARCHAR(100) NOT NULL,
        cost DECIMAL(10, 2) NOT NULL,
        coverage_percentage DECIMAL(3, 2) NOT NULL,
        FOREIGN KEY (actor_type_id) REFERENCES medical_actor_types(id)
    );
