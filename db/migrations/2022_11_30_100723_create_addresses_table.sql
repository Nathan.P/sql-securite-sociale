CREATE TABLE
    addresses (
        id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        city_id BIGINT NOT NULL,
        qpv_id BIGINT,
        street VARCHAR(100) NOT NULL,
        location POINT NOT NULL SRID 4326,
        FOREIGN KEY (qpv_id) REFERENCES qpvs(id),
        FOREIGN KEY (city_id) REFERENCES cities(id)
    );
