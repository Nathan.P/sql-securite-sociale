CREATE TABLE
    prestations (
        id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        medical_act_id BIGINT NOT NULL,
        person_id BIGINT NOT NULL,
        FOREIGN KEY (medical_act_id) REFERENCES medical_acts(id),
        FOREIGN KEY (person_id) REFERENCES persons(id)
    );
