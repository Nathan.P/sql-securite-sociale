CREATE TABLE
    employee_statutes (
        id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(100) NOT NULL,
        tax DECIMAL(3, 2) NOT NULL
    );
