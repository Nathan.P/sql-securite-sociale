CREATE TABLE
    cities (
      id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
      region_id BIGINT NOT NULL,
      name VARCHAR(100) NOT NULL,
      FOREIGN KEY (region_id) REFERENCES regions(id)
    );
