CREATE TABLE
    medical_actor_types (
        id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        type VARCHAR(100) NOT NULL
    );
