UPDATE addresses
SET addresses.qpv_id = (
    SELECT qpvs.id
    FROM qpvs
    WHERE
        ST_CONTAINS(qpvs.zone, addresses.location)
);


