SELECT
    medical_acts.name,
    COUNT(prestations.medical_act_id) as nb_prestations
FROM prestations
    INNER JOIN medical_acts ON prestations.medical_act_id = medical_acts.id
GROUP BY
    prestations.medical_act_id
ORDER BY
    COUNT(prestations.medical_act_id) DESC
LIMIT 3;


