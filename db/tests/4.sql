SELECT
    AVG(medical_acts.cost) as cost_average
FROM persons
    INNER JOIN prestations ON persons.id = prestations.person_id
    INNER JOIN medical_acts ON prestations.medical_act_id = medical_acts.id
WHERE
    persons.is_retired = true
    AND medical_acts.coverage_percentage = 1.00;

