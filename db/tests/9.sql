SELECT
    medical_acts.name,
    SUM(
        medical_acts.cost * medical_acts.coverage_percentage
    ) as total_cost
FROM prestations
    INNER JOIN medical_acts on prestations.medical_act_id = medical_acts.id
GROUP BY
    prestations.medical_act_id
ORDER BY total_cost desc
LIMIT 3;


