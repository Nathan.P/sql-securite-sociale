SELECT
    medical_actor_types.id,
    medical_actor_types.type,
    SUM(
        medical_acts.cost * medical_acts.coverage_percentage
    ) as total
FROM medical_actor_types
    INNER JOIN medical_acts ON medical_actor_types.id = medical_acts.actor_type_id
    INNER JOIN prestations ON medical_acts.id = prestations.medical_act_id
GROUP BY
    medical_actor_types.id;


