SELECT
    regions.id,
    regions.name,
    SUM(
        medical_acts.cost * medical_acts.coverage_percentage
    ) as total
FROM regions
    INNER JOIN cities ON regions.id = cities.region_id
    INNER JOIN addresses ON cities.id = addresses.city_id
    INNER JOIN persons ON addresses.id = persons.address_id
    INNER JOIN prestations ON persons.id = prestations.person_id
    INNER JOIN medical_acts ON prestations.medical_act_id = medical_acts.id
GROUP BY regions.id
ORDER BY total DESC
LIMIT 1;


