SELECT
    persons.id,
    persons.firstname,
    persons.lastname,
    IFNULL( (
            SUM(
                employee_salaries.salary * 0.159 + ( (
                        employee_salaries.salary - (
                            employee_salaries.salary * employee_statutes.tax
                        )
                    ) * 0.29
                )
            )
        ),
        0
    ) - IFNULL( (
            SELECT
                SUM(
                    medical_acts.cost * medical_acts.coverage_percentage
                )
            FROM prestations
                INNER JOIN medical_acts ON prestations.medical_act_id = medical_acts.id
            WHERE
                persons.id = prestations.person_id
        ),
        0
    ) AS total
FROM employees
    INNER JOIN employee_statutes ON employees.status_id = employee_statutes.id
    INNER JOIN employee_salaries ON employees.id = employee_salaries.employee_id
    INNER JOIN persons ON employees.id = persons.id
GROUP BY employees.id
ORDER BY employees.id;


