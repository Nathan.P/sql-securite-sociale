SELECT
    qpvs.id,
    qpvs.name,
    GROUP_CONCAT(DISTINCT medical_acts.name) as prestations,
    SUM(
        medical_acts.cost * medical_acts.coverage_percentage
    ) as costs
FROM qpvs
    INNER JOIN addresses ON qpvs.id = addresses.qpv_id
    INNER JOIN persons ON addresses.id = persons.address_id
    INNER JOIN prestations ON persons.id = prestations.person_id
    INNER JOIN medical_acts ON prestations.medical_act_id = medical_acts.id
GROUP BY qpvs.id


