SELECT
    regions.*,
    IFNULL( (
            SUM(
                employee_salaries.salary * 0.159 + ( (
                        employee_salaries.salary - (
                            employee_salaries.salary * employee_statutes.tax
                        )
                    ) * 0.29
                )
            )
        ),
        0
    ) as cotisation
FROM regions
    INNER JOIN cities ON regions.id = cities.region_id
    INNER JOIN addresses ON cities.id = addresses.city_id
    INNER JOIN companies ON addresses.city_id = companies.address_id
    LEFT JOIN employees ON companies.id = employees.company_id
    LEFT JOIN employee_statutes ON employees.status_id = employee_statutes.id
    LEFT JOIN employee_salaries ON employees.id = employee_salaries.employee_id
GROUP BY regions.id
ORDER BY cotisation DESC
LIMIT 1;