DROP DATABASE IF EXISTS __db__;
CREATE DATABASE IF NOT EXISTS __db__;
USE __db__;

DROP Table IF EXISTS employee_salaries;
DROP Table IF EXISTS employees;
DROP Table IF EXISTS employee_statutes;
DROP Table IF EXISTS retirees;
DROP Table IF EXISTS companies;
DROP Table IF EXISTS prestations;
DROP Table IF EXISTS medical_acts;
DROP Table IF EXISTS medical_actor_types;
DROP Table IF EXISTS persons;
DROP Table IF EXISTS qpvs;
DROP Table IF EXISTS addresses;
DROP Table IF EXISTS cities;
DROP Table IF EXISTS regions;