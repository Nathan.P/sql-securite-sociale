# Export env vars
export $(grep -v '^#' .env.local | xargs);

# Import Init.sql
cat ./db/init.sql >> init.sql;

# Import all migrations
for migration in ./db/migrations/*;
  do cat $migration >> init.sql;
done;

# Import all seeds
for seed in ./db/seeds/*;
  do cat $seed >> init.sql;
done;

sed -i "s/__db__/${MYSQL_DATABASE}/" init.sql;
mysql --user=${MYSQL_USER} --password=${MYSQL_PASSWORD} < 'init.sql';

# Remove generated file
#rm init.sql;