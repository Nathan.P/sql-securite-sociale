#!/bin/bash
docker-compose up -d;

# Export env vars
export $(grep -v '^#' .env.local | xargs)

while ! docker-compose logs db | grep "InnoDB initialization has started"; do sleep 1; done

# Import Init.sql
cat ./db/init.sql >> init.sql

# Import all migrations
for migration in ./db/migrations/*;
  do cat $migration >> init.sql
done;

# Import all seeds
for seed in ./db/seeds/*;
  do cat $seed >> init.sql
done;

sed -i -e "s/__db__/${MYSQL_DATABASE}/" init.sql;
docker-compose cp ./init.sql db:./
docker-compose exec db /bin/bash -c "mysql --user=${MYSQL_USER} --password=${MYSQL_PASSWORD} --database=${MYSQL_DATABASE} < 'init.sql'"

# Remove generated file
rm init.sql
rm init.sql-e